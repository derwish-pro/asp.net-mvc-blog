﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Blog.Models;
using PagedList;

namespace Blog.Controllers
{
    public class CommentsController : Controller
    {




        private BlogContext db = new BlogContext();

        public ActionResult Index()
        {
            return RedirectToAction("EditList");
        }

        public ActionResult EditList(int? id, int? page)
        {
            //количество сообщений на одну страницу
            int pageSize = 3;

            int pageNumber = page ?? 1;

            int count = db.Comments.Count();
            if (page == null && count > 0) pageSize = count;

            var comments = db.Comments
                .OrderByDescending(p => p.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .Include(c => c.Post)
                .ToList();

            var pagedComments = new StaticPagedList<Comment>(comments, pageNumber, pageSize, count);

            ViewBag.AllCommentsCount = count;
            ViewBag.NewCommentsCount = db.Comments.Count(c => c.IsSeenByAdmin == false);
            ViewBag.Title = "Все комментарии";
            return View(pagedComments);
        }

        public ActionResult EditListNewOnly(int? id, int? page)
        {
            //количество сообщений на одну страницу
            int pageSize = 3;

            int pageNumber = page ?? 1;

            int count = db.Comments
                .Count(c => c.IsSeenByAdmin == false);
            if (page == null && count > 0) pageSize = count;

            var comments = db.Comments
                .Where(c => c.IsSeenByAdmin == false)
                .OrderByDescending(p => p.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .Include(c => c.Post)
                .ToList();

            var pagedComments = new StaticPagedList<Comment>(comments, pageNumber, pageSize, count);

            ViewBag.AllCommentsCount = db.Comments.Count();
            ViewBag.NewCommentsCount = count;
            ViewBag.Title = "Новые комментарии";
            return View("EditList", pagedComments);
        }

        public ActionResult EditListPartial(int? id, int? page)
        {
            //количество сообщений на одну страницу
            int pageSize = 3;

            if (id == null) return HttpNotFound();

            int pageNumber = page ?? 1;
            int count = db.Comments.Count(c => c.PostId == id);
            if (page == null && count > 0) pageSize = count;

            var comments = db.Comments
                .Where(c => c.PostId == id)
                .OrderByDescending(p => p.Id)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var pagedComments = new StaticPagedList<Comment>(comments, pageNumber, pageSize, count);

            return PartialView("_EditList", pagedComments);
        }



        public ActionResult ListPartial(int? id, int? page)
        {
            //количество сообщений на одну страницу
            int pageSize = 3;

            int pageNumber = page ?? 1;

            if (id == null) return HttpNotFound();

            int count = db.Comments
                    .Where(c => c.PostId == id)
                    .Count(c => c.IsAllowedByAdmin == true);
  
            if (page == null && count>0) pageSize = count;

              var comments = db.Comments
                    .Where(c => c.PostId == id)
                    .Where(c => c.IsAllowedByAdmin == true)
                    .OrderByDescending(p => p.Id)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

            var pagedComments = new StaticPagedList<Comment>(comments, pageNumber, pageSize, count);

            return PartialView("_List", pagedComments);
        }

      


        public ActionResult Create()
        {
           Comment comment = new Comment
           {
               Date = DateTime.Now,
               IsAllowedByAdmin = true,
               IsSeenByAdmin = true
           };
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            return View(comment);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Author,PostId,IsSeenByAdmin,IsAllowedByAdmin,Date,Text")] Comment comment)
        {
            Post post = db.Posts.FirstOrDefault(p => p.Id == comment.PostId);
            if (post == null) ModelState.AddModelError("PostId", "Не найден пост, соответствующий комментарию.");
            else
            {
                comment.Post = post;
                ModelState["Post"].Errors.Clear();
                TryUpdateModel(comment);
            }
                if (comment.Date == null) comment.Date = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            return View(comment);

        }

        public ActionResult CreatePartial(int? id)
        {
            if (id == null) return HttpNotFound();

            Post post = db.Posts.FirstOrDefault(p => p.Id == id);

            if (post == null) return HttpNotFound();

            Comment comment = new Comment { PostId = id };
            return PartialView("_Create", comment);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePartial([Bind(Include = "Author,PostId,Text")] Comment comment)
        {
            //временной интервал (секунд) в течении которого невозможна повторная отправка комментариев
            const int commentsDelay = 10;

            Post post = db.Posts.FirstOrDefault(p => p.Id == comment.PostId);
            if (post == null) ModelState.AddModelError("", "Не найден пост, соответствующий комментарию.");
            else
            {
                comment.Post = post;
                ModelState["Post"].Errors.Clear();
                TryUpdateModel(comment);
            }


            comment.Date = DateTime.Now;


            Comment lastComment = db.Comments.Where(c => c.Author == comment.Author).OrderByDescending(c=>c.Date).FirstOrDefault();
            if (lastComment != null)
            {
                if (lastComment.Date > DateTime.Now.AddSeconds(-commentsDelay))
                    ModelState.AddModelError("", "В целях безопасности введено ограничение: не более 1 комментария в "
                                                 + commentsDelay + " секунд. Подождите "
                                                 +
                                                 (lastComment.Date - DateTime.Now.AddSeconds(-commentsDelay)).Value
                                                     .Seconds
                                                 + " секунд и повторите.");

            }

            if (ModelState.IsValid)
            {
                if (post.AutoExceptComments)
                    comment.IsAllowedByAdmin = true;
                else
                    comment.IsAllowedByAdmin = false;

                comment.IsSeenByAdmin = false;

                db.Comments.Add(comment);
                db.SaveChanges();

                if (post.AutoExceptComments)
                    return Content("Ваш комментарий опубликован. Спасибо.");
                else
                    return Content("Ваш комментарий принят и будет обработан администратором. Спасибо.");
            }

            return PartialView("_Create", comment);
        }


        public ActionResult Edit(int? id)
        {
            if (id == null) return HttpNotFound();

            Comment comment = db.Comments.FirstOrDefault(c => c.Id == id);
            if (comment == null) return HttpNotFound();
   
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            return View(comment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Author,PostId,IsSeenByAdmin,IsAllowedByAdmin,Date,Text")] Comment comment)
        {
            Post post = db.Posts.FirstOrDefault(p => p.Id == comment.PostId);
            if (post == null) ModelState.AddModelError("PostId", "Не найден пост, соответствующий комментарию.");
            else
            {
                comment.Post = post;
                ModelState["Post"].Errors.Clear();
                TryUpdateModel(comment);
            }
            if (comment.Date == null) comment.Date = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            return View(comment);
        }

  
        public ActionResult Delete(int? id)
        {
            if (id == null) return HttpNotFound();

            Comment comment = db.Comments.FirstOrDefault(c => c.Id == id);
            if (comment == null) return HttpNotFound();

            db.Comments.Remove(comment);
            db.SaveChanges();

            return RedirectToAction("EditList");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Allow(int id)
        {
            if (id == null) return HttpNotFound();

            Comment comment = db.Comments.Include(c => c.Post).FirstOrDefault(c => c.Id == id);
            if (comment == null) return HttpNotFound();
            comment.IsAllowedByAdmin = true;

            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
