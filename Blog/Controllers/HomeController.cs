﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;
using PagedList;
using PagedList.Mvc;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
    
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Posts");
        }
    }
}