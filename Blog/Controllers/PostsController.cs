﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Blog.Models;
using PagedList;

namespace Blog.Controllers
{
    public class PostsController : Controller
    {
        private BlogContext db = new BlogContext();



        public ActionResult Index(int page = 1)
        {
            int pageSize = 3;

            int count = db.Posts.Count();
            var posts = db.Posts.OrderByDescending(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var pagedPosts = new StaticPagedList<Post>(posts, page, pageSize, count);

            return View(pagedPosts);
        }

        // GET: Posts
        public ActionResult List(int page = 1)
        {
            int pageSize = 10;

            int count = db.Posts.Count();
            var posts = db.Posts.OrderByDescending(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var pagedPosts = new StaticPagedList<Post>(posts, page, pageSize, count);

            ViewBag.PostsCount = count;
            return View(pagedPosts);
        }

        public ActionResult Show(int? id)
        {
            if (id == null) return HttpNotFound();

            Post post = db.Posts.FirstOrDefault(p => p.Id == id);

            if (post == null) return HttpNotFound();
            return View(post);
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return HttpNotFound();

            Post post = db.Posts.FirstOrDefault(p => p.Id == id);

            if (post == null) return HttpNotFound();
            return View(post);
        }



        public ActionResult Edit(int? id)
        {
            if (id == null) return HttpNotFound();

            Post post = db.Posts.FirstOrDefault(p => p.Id == id);

            if (post == null) return HttpNotFound();
            return View(post);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,Title,Description,Text,AllowComments")] Post post)
        {
            if (ModelState.IsValid)
            {
                if (post.Date == null) post.Date = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(post);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return HttpNotFound();

            Post post = db.Posts.FirstOrDefault(p => p.Id == id);

            if (post == null) return HttpNotFound();
            return View(post);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("List");
        }

        public ActionResult Create()
        {
            Post post = new Post();
            post.Date = DateTime.Now;
            post.AllowComments = true;
            post.AutoExceptComments = false;

            return View(post);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Date,Title,Description,Text,AllowComments")] Post post)
        {
            if (ModelState.IsValid)
            {
                if (post.Date == null) post.Date = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(post);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();

            base.Dispose(disposing);
        }
    }
}
