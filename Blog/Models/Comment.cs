﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Profile;

namespace Blog.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [DisplayName("Дата")]
        public DateTime? Date { get; set; }

        [DisplayName("Автор")]
        [Required(ErrorMessage = "Не указано имя автора")]
        public string Author { get; set; }

        [DisplayName("Комментарий")]
        [Required(ErrorMessage = "Не набран текст комментария")]
        public string Text { get; set; }

        [DisplayName("Пост")]
        public int? PostId { get; set; }

        [Required]
        public Post Post { get; set; }

        [DisplayName("Просмотрено администратором")]
        public bool IsSeenByAdmin { get; set; }

        [DisplayName("Одобрено администратором")]
        public bool IsAllowedByAdmin { get; set; }
    }
}