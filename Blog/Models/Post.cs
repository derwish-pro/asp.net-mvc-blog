﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        public int Id { get; set; }

        [DisplayName("Дата")]
        public DateTime? Date { get; set; }

        [DisplayName( "Заголовок")]
        [StringLength(100, ErrorMessage = "Заголовок не может привышать 100 символов")]
        [Required(ErrorMessage = "Не набран заголовок поста")]
        public string Title { get; set; }

        [DisplayName("Описание")]
        [Required(ErrorMessage = "Не набрано описание поста")]
        public string Description { get; set; }


        [DisplayName("Текcт")]
        [Required(ErrorMessage = "Не набран текст поста")]
        public string Text { get; set; }


        public IEnumerable<Comment> Comments;

        [DisplayName("Разрешить комментарии")]
        [Required]
        public bool AllowComments { get; set; }

        [DisplayName("Автоматически добавлять комментарии без проверки админом")]
        [Required]
        public bool AutoExceptComments { get; set; }
    }
}